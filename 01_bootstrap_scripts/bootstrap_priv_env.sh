#!/bin/bash
##### This is to bootstrap my linux environment ######
## Arch Base installation

echo "Installing ntfs-3g to ensure, ntfs harddrives can be read"
pacman -Syyu ntfs-3g

echo "Installing bash-completion if we're on bash"
if [ $SHELL=="/bin/bash" ]
then
    pacman -S bash-completion 
else
    echo "Not installed, shell is: "$SHELL
fi

echo "Installing shared clipboard, drag'n'drop etc with guest"
pacman -S spice-vdagent

echo "Installing yay"
sudo pacman -S git
cd /opt
sudo git clone https://aur.archlinux.org/yay-git.git
sudo chown -R $USER:$USER ./yay-git
cd yay-git
makepkg -si
cd ~

echo "Copy home, preserving ownerships"
cp -rp /run/media/$USER/Elements/Martin/Backup/home ~ 

echo "config gnome" 
dconf load / < ~/Documents/Backup/arch_gnome_settings
# config grub

#echo "Config files"
#mkdir -p ~/Software/SoftwareDev && cd ~/Software/SoftwareDev
#git clone https://gitlab.com/codestumbler/configs

echo "Install and setup zsh and p10k"
if [ $SHELL=="/bin/zsh" ]
then
else
    sudo pacman -S zsh zsh-autosuggestions zsh-completions zsh-theme-powerlevel10k
    chsh -s /bin/zsh
    echo "restart the Terminal"
fi

echo "Configuration of the zsh with p10k not yet clear. "
#ln -s .zshrc ~/.zshrc
#ln -s .oh-my-zsh ~/.oh-my-zsh
#ln -s .p10k.zsh ~/.p10k.zsh
#source ~/.zshrc

echo "Download and install p10k recommended fonts"
mkdir -p ~/.local/share/fonts && cd ~/.local/share/fonts
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf
fc-cache -f -v
cd ~
echo "Restart the terminal now."

echo "Setup vim "
ln -s ~/Software/SoftwareDev/configs/.vimrc ~/.vimrc
ln -s ~/Software/SoftwareDev/configs/.vim ~/.vim

echo "Install Programs. Ensure file list is present"
sudo pacman -S - < ~/Documents/Backup/arch_installed_software.txt

echo "Program Configurations"
yay -S vim-latexsuite


echo "If scrolling doesn't work, run the code below"
#sudo touch /etc/X11/xorg.conf.d/100-touchpad.conf
#cat <<EOT >> /etc/X11/xorg.conf.d/100-touchpad.conf
#Section "InputClass"
#        Identifier "libinput touchpad catchall"
#        MatchIsTouchpad "on"
#        MatchDevicePath "/dev/input/event*"
#        Driver "libinput"
#        Option "Tapping" "on"
#        Option "NaturalScrolling" "false"
#        Option "ClickMethod" "clickfinger"
#        Option "TappingButtonMap" "lrm"
#        Option "DisableWhileTyping" "true"
#EndSection
#EOT

