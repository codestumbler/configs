" =================================================
" === Martin's .vimrc  
" =================================================

" -------------------------------------------------
" --- Basic settings  
" -------------------------------------------------

" be iMproved, required
set nocompatible 

"Define local leader
let mapleader ="," 

" required
filetype off     

" use modelines
set modelines=1

" Avoid annoying error bell
set belloff=all

" improves the % command; not backwards compatible.
packadd matchit 

" -------------------------------------------------
" --- Integrations  
" -------------------------------------------------

" copy to system clipboard
set clipboard=unnamedplus

" Puts all backup/swap/undo files in their respective folder
set backupdir=~/.vim/backup 
set directory=~/.vim/swap/  
set undodir=~/.vim/undo/

" Toggle paste mode to avoid autoindent during pasting 
set pastetoggle=ttp

" Change directory to the current buffer when opening files. 
set autochdir               

" Automatically write file when leaving a modified buffer
set autowrite   

" Sets ctags lookup dir
set tags=tags;/

" -------------------------------------------------
" --- Display  
" -------------------------------------------------

" load indent files, to automatically do language-dependent indenting.
filetype plugin indent on

" Switch syntax highlighting on
syntax enable        

" switch on highlighting the last used search pattern.
set hlsearch    

" Display Row numbers
set number      

" Show info in the window title
set title       

" indent next line in code
set autoindent              

" number of visual spaces per tab 4 space indentation"
set tabstop=4               

" number of spaces to use for autoindent
set shiftwidth=4            

" number of spaces to insert/remove on <TAB>
set softtabstop=4           

" insert spaces instead of tabs"
set expandtab               

" show bracket counterpart
set showmatch               

"show vertical indent lines 
set listchars=tab:\|\ 
set list
set cursorcolumn
set cursorline

"Line length above which to break a line
autocmd FileType python,cpp,c,rst,sh,sli setl textwidth=79 
autocmd FileType markdown,text setl textwidth=130

" Disables automatic commenting on newline:
set formatoptions-=cro

" Hitting enter in command mode after a search will clear the search pattern
noremap <CR> :noh<CR><CR>
" -------------------------------------------------
" --- Navigation  
" -------------------------------------------------

" Search down into subfolders; tab-completion for all file-related tasks
set path+=** 

" Better command-line completion
set wildmenu    

" For buffers
nnoremap <Leader>bc :bp\|bd #<CR>

" Allows to escape insert mode without leaving home row."
imap kj <Esc>
imap jk <Esc>

" inserts blank line above 
nnoremap <leader>j o<ESC>k  

" inserts blank line below 
nnoremap <leader>k O<ESC>j  

" Enable folding with spacebar
set foldmethod=indent
set foldlevel=99
nnoremap <space> zR


" -------------------------------------------------
" --- Plugins with Vundle  
" -------------------------------------------------

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'           

" insert text/code snippets
Plugin 'SirVer/ultisnips'               

" Color scheme
Plugin 'ayu-theme/ayu-vim'              

" use YouCompleteMe
Plugin 'Valloric/YouCompleteMe'         

" aligning shtuff
Plugin 'godlygeek/tabular'              

" Useful Vim status bar
Plugin 'vim-airline/vim-airline'        
Plugin 'vim-airline/vim-airline-themes'

" fuzzy file search with ctrlp
Plugin 'ctrlpvim/ctrlp.vim'             

" i don't think i have used this guy so far
Plugin 'tpope/vim-surround'

" A Vim Plugin for Lively Previewing LaTeX PDF Output
Plugin 'xuhdev/vim-latex-live-preview'

" All Plugins must be added before the following line
call vundle#end()            

" -------------------------------------------------
" --- /Plugins with Vundle  
" -------------------------------------------------

""==================Plugin Settings================================

" -------------------------------------------------
" --- Vim-Airline  
" -------------------------------------------------

let g:airline_theme='dark'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = '>'
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

" -------------------------------------------------
" --- YouCompleteMe  
" -------------------------------------------------

" Enabbling YCM for Python 
let g:ycm_python_interpreter_path = ''
let g:ycm_python_sys_path = [] 
let g:ycm_extra_conf_vim_data = [
    \ 'g:ycm_python_interpreter_path',
    \ 'g:ycm_python_sys_path'
    \]
let g:ycm_global_ycm_extra_conf = '~/global_extra_conf.py'

" make YCM compatible with UltiSnips (using supertab)
let g:ycm_autoclose_preview_window_after_completion=0
let g:ycm_key_list_select_completion = ['<C-n>']
let g:ycm_key_list_previous_completion = ['<C-p>']
let g:SuperTabDefaultCompletionType = '<C-n>'

" Go to declaration with ,g 
map <leader>g :YcmCompleter GoToDefinitionElseDeclaration<CR> 

" YCM and Latex 
"let g:ycm_semantic_triggers.tex = [
"      \ 're!\\[A-Za-z]*cite[A-Za-z]*(\[[^]]*\]){0,2}{[^}]*',
"      \ 're!\\[A-Za-z]*ref({[^}]*|range{([^,{}]*(}{)?))',
"      \ 're!\\hyperref\[[^]]*',
"      \ 're!\\includegraphics\*?(\[[^]]*\]){0,2}{[^}]*',
"      \ 're!\\(include(only)?|input){[^}]*',
"      \ 're!\\\a*(gls|Gls|GLS)(pl)?\a*(\s*\[[^]]*\]){0,2}\s*\{[^}]*',
"      \ 're!\\includepdf(\s*\[[^]]*\])?\s*\{[^}]*',
"      \ 're!\\includestandalone(\s*\[[^]]*\])?\s*\{[^}]*',
"      \ ]
" -------------------------------------------------
" --- UltiSnips  
" -------------------------------------------------

let g:UltiSnipsExpandTrigger="<leader><TAB>"
let g:UltiSnipsJumpForwardTrigger="<leader><TAB>"
let g:UltiSnipsEditSplit="horizontal"

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Reload snippet file after edit
map <leader>uu :call UltiSnips#RefreshSnippets()<CR><CR>

" -------------------------------------------------
" --- Ctrl P 
" -------------------------------------------------

let g:ctrlp_show_hidden = 1
set runtimepath^=~/.vim/bundle/ctrlp.vim

" -------------------------------------------------
" --- Vim-latex suite and Live Preview
" -------------------------------------------------

set grepprg=grep\ -nH\ $*
let g:tex_flavor = "latex"

" speed up insertion of \item on next line
imap <leader>li <CR>\item 

" compilation rule: PDF
let g:Tex_DefaultTargetFormat='pdf'

" viewing rule for PDF
"let g:Tex_ViewRule_PDF

nnoremap <leader>lx :!latexmk -xelatex <CR><CR>

" use mupdf for live preview 
let g:livepreview_previewer = 'evince'
set updatetime=1500
nnoremap <leader>lp :LLPStartPreview <CR><CR>

"==================/Plugin Settings==============================

" -------------------------------------------------
" --- netrw settings
" -------------------------------------------------

let g:netrw_liststyle    = 3  " Tree-view
let g:netrw_winsize      = 20 " Default width of the netrw explorer window
let g:netrw_banner       = 0  " Remove Header in netrw
let g:netrw_browse_split = 4  " Open files in new vertical split
let g:netrw_altv         = 1  " Keep netrw in the small pane, open file in the wide

" Create Buffer with netrw at startup
augroup ProjectDrawer
  autocmd!
  autocmd VimEnter * :Vexplore
augroup END

" Automatically close vim if netrw is the last open buffer
aug netrw_close
  au!
  au WinEnter * if winnr('$') == 1 && getbufvar(winbufnr(winnr()), "&filetype") == "netrw"|q|endif
aug END

" -------------------------------------------------
" --- /netrw settings
" -------------------------------------------------


