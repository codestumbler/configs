#!/bin/bash

# 1) place .bashrc, .vimrc, .tmux.conf in /home/ 
if echo $0=='/bin/bash'
then
    ln -s .bashrc $HOME
    source ~/.bashrc
else
    ln -s .zsh $HOME
    exec zsh
fi

ln -s .vim/ $HOME
# 2) set up swap, undo and backup folders
mkdir -p .vim/swap .vim/undo .vim/backup

# 2) install vim 

# Prepare debian WSL
if [[ $(grep Microsoft /proc/version) ]]; then
    sudo apt update && sudo apt install libncurses5-dev libatk1.0-dev
    python3-dev ruby-dev lua5.3-0 lua5.3-dev libperl-dev git build-essential
    # Clone and install vim
    git clone https://github.com/vim/vim.git ~/
    cd ~/vim/

    ./configure --prefix=/usr/local \
         --enable-python3interp \
         --with-python3-config-dir=/usr/lib/python3.6/config-*
    make
    sudo make install
fi
# 3) install vim plugins
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

# 3.1) You Complete Me; run :PluginInstall in vim
str1 = $(uname -r)
str2 = $(uname -v)
if [[$str1 == *"MANJARO"]]
then
    pacman -Syyu cmake vim-nox python3-dev golang npm nodejs default-jdk 
else
apt install build-essential cmake vim-nox python3-dev mono-complete golang npm nodejs default-jdk
fi

# install go language if apt install golang fails
#wget -c hs://golang.org/dl/go1.15.2.linux-amd64.tar.gz 
#shasum -a 256 go1.15.2.linux-amd64.tar.gz
#sudo tar -C /usr/local -xvzf go1.15.2.linux-amd64.tar.gz
#mkdir -p ~/go_projects/{bin,src,pkg}
#cd ~/go_projects/
#export  PATH=$PATH:/usr/local/go/bin
#export GOPATH="$HOME/go_projects
#export GOBIN="$GOPATH/bin
#source ~/.profile

cd ~/.vim/bundle/YouCompleteMe
python3 install.py --all

# 4) set up documents and software folder structure
# 5) set up ssh
#ssh-keygen -t  

# 6) clone repos
# 7) install doxygen 
sudo apt install doxygen
# in project folder run: doxygen -g dconfig

# 8) install ctags
sudo apt install exuberant-ctags
# in project folder run: ctags -R --exclude=.git .
